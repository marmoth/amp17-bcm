package de.oth.amp17.bff.bcm.testing;

import org.camunda.spin.Spin;
import org.camunda.spin.json.SpinJsonNode;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.time.Instant;

@Path("/ping")
public class Ping {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String timestamp() {
        return Instant.now().toString();
    }

    @GET
    @Path("spin")
    public String spin() {
        SpinJsonNode root = Spin.JSON("{}");
        root.prop("timestamp", timestamp());
        return root.toString();
    }

}
