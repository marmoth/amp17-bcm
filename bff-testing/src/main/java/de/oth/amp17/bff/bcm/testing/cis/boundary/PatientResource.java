package de.oth.amp17.bff.bcm.testing.cis.boundary;

import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.param.DateRangeParam;
import de.oth.amp17.bff.bcm.testing.cis.control.JsonMonitoringStatus;
import de.oth.amp17.bff.bcm.testing.cis.control.JsonPatient;
import de.oth.amp17.bff.bcm.testing.cis.control.JsonResourceIdentity;
import org.camunda.spin.Spin;
import org.camunda.spin.json.SpinJsonNode;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.instance.model.api.IIdType;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.*;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

//@Stateless
@ApplicationScoped
@Path("/cis/patient")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
//@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class PatientResource {

    private static final Logger LOG = Logger.getLogger(PatientResource.class.getName());

    @Context
    UriInfo uriInfo;

    @Inject
    IGenericClient client;

    @Inject
    IParser parser;

    public PatientResource() {
    }

//    public PatientResource(IGenericClient client) {
//        this.client = client;
//    }

    @POST
    @Path("bcmRequest")
    public Response createMonitoringRequest(JsonObject request) {
        JsonNumber patientId = request.getJsonNumber("patientId");

        ProcedureRequest procedureRequest = new ProcedureRequest();
        procedureRequest.setStatus(ProcedureRequest.ProcedureRequestStatus.ACTIVE);
        procedureRequest.setSubject(
                new Reference(
                        new IdType(ResourceType.Patient.name(), patientId.longValue())));
        String requestToken = UUID.randomUUID().toString();
        procedureRequest.setCode(
                new CodeableConcept()
                        .addCoding(
                                new Coding("http://oth-regensburg.de/amp/procedure", "bcm", "Blutzucker-Monitoring"))
                        .addCoding(
                                new Coding("http://oth-regensburg.de/amp/bcm/token", requestToken, "Token: " + requestToken)));

        MethodOutcome outcome = client.create()
                .resource(procedureRequest)
                .execute();

        if (!outcome.getCreated()) {
            return Response.serverError().entity(outcome.getOperationOutcome()).build();
        }

        return Response.created(
                uriInfo.getRequestUriBuilder().segment(outcome.getId().getIdPart()).build())
                .entity(Json.createObjectBuilder()
                        .add("state", "requested")
                        .add("requestToken", requestToken)
                        .add("identity", new JsonResourceIdentity(outcome.getId()).toJsonObjectBuilder())
                        .build())
                .build();

    }

    @GET
    @Path("registeredToday")
    public Response registeredToday() {
        Bundle bundle = client.search()
                .forResource(Patient.class)
                .lastUpdated(new DateRangeParam(LocalDate.now().toString(), null))
                .sort().descending("_lastUpdated")
                .returnBundle(Bundle.class)
                .execute();
        return patientBundleToResponse(bundle);
    }

    @GET
    @Path("updatedRecently")
    public Response updatedRecently() {
        Bundle bundle = client.search()
                .forResource(Patient.class)
                .lastUpdated(new DateRangeParam(LocalDate.now().minusDays(14).toString(), null))
                .sort().descending("_lastUpdated")
                .count(20)
                .returnBundle(Bundle.class)
                .execute();
        return patientBundleToResponse(bundle);
    }

    private Response patientBundleToResponse(Bundle bundle) {
        JsonArray patients = bundle.getEntry().stream()
                .map(JsonPatient::from)
                .map(JsonPatient::toJsonObjectBuilder)
                .collect(Json::createArrayBuilder, JsonArrayBuilder::add, JsonArrayBuilder::add)
                .build();
        return Response.ok(patients).build();
    }

    @GET
    @Path("{id}")
    public String patientById(@PathParam("id") Long id) {
        Patient patient = client.read()
                .resource(Patient.class)
                .withId(id)
                .execute();
        return new JsonPatient(patient).toJson().toString();
    }

    @GET
    @Path("{id}/bcm")
    public JsonObject monitoringStateForPatient(@PathParam("id") String patientId) {
        Bundle bundle = client.search()
                .forResource(ProcedureRequest.class)
                .where(ProcedureRequest.PATIENT.hasId(patientId))
                .where(ProcedureRequest.CODE.exactly().systemAndCode("http://oth-regensburg.de/amp/procedure", "bcm"))
                .revInclude(Procedure.INCLUDE_BASED_ON)
                .include(ProcedureRequest.INCLUDE_PATIENT)
                .returnBundle(Bundle.class)
                .execute();
        LOG.info("FHIR :: fetched BCM state, request: " + bundle.getLink("self").getUrl());
        return new JsonMonitoringStatus(bundle).toJson();
    }

//    @GET
//    @Path("{id}")
//    public JsonObject patientById(@PathParam("id") String id) {
//        Patient patient = client.read()
//                .resource(Patient.class)
//                .withId(id)
//                .execute();
//        return new JsonPatient(patient).toJson();
//    }

    @POST
    public Response registerPatient(SpinJsonNode data) {

        Patient patient = new Patient();
        patient.setActive(true);
        patient.setBirthDateElement(extractDateOfBirth(data));
        patient.setName(extractName(data));

        MethodOutcome outcome = client.create().resource(patient).execute();

        Bundle bundle = new Bundle();
        bundle.addEntry().setResource((Resource) outcome.getResource());
        bundle.addEntry().setResource((Resource) outcome.getOperationOutcome());

        boolean created = outcome.getCreated();
        if (!created) {
            return Response.serverError().entity(bundle).build();
        }

        IIdType outcomeId = outcome.getId();

        LOG.info("CREATED :: " + outcomeId.toString());

        patient.setId(outcomeId);

        return Response.created(uriInfo.getRequestUri())
                .entity(new JsonPatient(patient).toJson())
                .build();

    }

    private DateType extractDateOfBirth(SpinJsonNode data) {
        return new DateType(data.prop("dateOfBirth").stringValue());
    }

    private List<HumanName> extractName(SpinJsonNode data) {
        String firstname = data.prop("firstname").stringValue();
        String lastname = data.prop("lastname").stringValue();

        HumanName name = new HumanName();
        name.setFamily(lastname);
        name.addGiven(firstname);

        return Collections.singletonList(name);
    }


    @GET
    public SpinJsonNode get() {
        return Spin.JSON("{}").prop("test", "test");
    }

    private SpinJsonNode emptyJsonObject() {
        return Spin.JSON(Collections.emptyMap());
    }
}
