package de.oth.amp17.bff.bcm.testing.cis.control;

import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Procedure;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

public class JsonProcedureStatus implements JsonView {

    private final Patient patient;

    private final Procedure procedure;

    public JsonProcedureStatus(Patient patient, Procedure procedure) {
        this.patient = patient;
        this.procedure = procedure;
    }

    @Override
    public JsonObjectBuilder toJsonObjectBuilder() {
        return Json.createObjectBuilder()
                .add("patient", new JsonPatient(patient).toJsonObjectBuilder())
                .add("procedure", procedureToJson());
    }

    private JsonObjectBuilder procedureToJson() {
        return Json.createObjectBuilder()
                .add("token", extractToken())
                .add("identity", new JsonResourceIdentity(procedure).toJsonObjectBuilder());
    }

    private String extractToken() {
        return procedure.getCode().getCoding().stream()
                .filter(each -> "http://oth-regensburg.de/amp/bcm/token".equals(each.getSystem()))
                .map(Coding::getCode)
                .findFirst().get();
    }
}
