package de.oth.amp17.bff.bcm.testing;

import org.camunda.spin.Spin;
import org.camunda.spin.json.SpinJsonNode;

import javax.enterprise.context.ApplicationScoped;
import javax.mail.internet.ContentType;
import javax.mail.internet.ParseException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import java.io.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

@Provider
//@ApplicationScoped
public class SpinMessageBody implements MessageBodyReader<SpinJsonNode>, MessageBodyWriter<SpinJsonNode> {

    private static final String PARAM_CONTENT_TYPE_CHARSET = "charset";
    private static final String UTF_8 = "UTF-8";

    @Override
    public boolean isReadable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return true;
    }

    @Override
    public SpinJsonNode readFrom(Class<SpinJsonNode> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, String> httpHeaders, InputStream entityStream) throws IOException, WebApplicationException {
        String charset = extractCharSet(httpHeaders);
        InputStreamReader reader = new InputStreamReader(entityStream, charset);
        return Spin.JSON(reader);
    }

    private String extractCharSet(MultivaluedMap<String, String> httpHeaders) throws IOException {
        String contentTypeHeader = httpHeaders.getFirst(HttpHeaders.CONTENT_TYPE);
        if (contentTypeHeader == null) {
            return UTF_8;
        }
        ContentType contentType;
        try {
            contentType = new ContentType(contentTypeHeader);
        } catch (ParseException e) {
            throw new IOException(e);
        }
        String charset = contentType.getParameter(PARAM_CONTENT_TYPE_CHARSET);
        if (charset == null) {
            return UTF_8;
        }
        return charset;
    }

    @Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return true;
    }

    @Override
    public long getSize(SpinJsonNode spinJsonNode, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return -1;
    }

    @Override
    public void writeTo(SpinJsonNode spinJsonNode, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream) throws IOException, WebApplicationException {
        httpHeaders.putSingle(HttpHeaders.CONTENT_TYPE, mediaType.withCharset(UTF_8).toString());
        spinJsonNode.writeToWriter(new OutputStreamWriter(entityStream, UTF_8));
    }
}
