package de.oth.amp17.bff.bcm.testing;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.util.logging.Logger;

@Provider
public class ResponseCharsetSettingFilter implements ContainerResponseFilter {

    private static final Logger LOG = Logger.getLogger(ResponseCharsetSettingFilter.class.getName());

    @Override
    public void filter(
            final ContainerRequestContext requestContext,
            final ContainerResponseContext responseContext) throws IOException {

        final MediaType mediaType = responseContext.getMediaType();
        if (mediaType == null) {
            return;
        }

        final String contentTypeString = responseContext.getHeaderString(HttpHeaders.CONTENT_TYPE);
        if (contentTypeString != null) {
            final MediaType contentType;
            try {
                contentType = MediaType.valueOf(contentTypeString);
            } catch(IllegalArgumentException ex) {
                LOG.warning(String.format("Illegal Content-Type: '%s'; request path: %s", contentTypeString, requestContext.getUriInfo().getPath()));
                return;
            }
            if(mediaType.getParameters().containsKey(MediaType.CHARSET_PARAMETER)) {
                return;
            }
        }


        if (MediaType.APPLICATION_JSON_TYPE.isCompatible(mediaType)) {
            MediaType withCharset = mediaType.withCharset("UTF-8");
            responseContext.getHeaders().putSingle(HttpHeaders.CONTENT_TYPE, withCharset);
            LOG.finer(String.format("Adjusted Content-Type %s to %s", contentTypeString, withCharset.toString()));
        }
    }
}
