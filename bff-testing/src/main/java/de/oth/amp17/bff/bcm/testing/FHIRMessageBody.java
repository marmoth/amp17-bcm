package de.oth.amp17.bff.bcm.testing;

import ca.uhn.fhir.parser.IParser;
import org.hl7.fhir.instance.model.api.IBaseResource;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

@Provider
//@ApplicationScoped
public class FHIRMessageBody implements MessageBodyWriter<IBaseResource> {

    @Inject
    private IParser parser;

    public FHIRMessageBody() {
    }

    public FHIRMessageBody(IParser parser) {
        this.parser = parser;
    }

    @Override
    public boolean isWriteable(Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return true;
    }

    @Override
    public long getSize(IBaseResource resource, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType) {
        return -1;
    }

    @Override
    public void writeTo(IBaseResource resource, Class<?> type, Type genericType, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> httpHeaders, OutputStream entityStream) throws IOException, WebApplicationException {
        String charset = mediaType.getParameters().get(MediaType.CHARSET_PARAMETER);
        charset = charset == null ? "UTF-8" : charset;
        httpHeaders.putSingle(HttpHeaders.CONTENT_TYPE, mediaType.withCharset(charset).toString());
        parser.encodeResourceToWriter(resource, new OutputStreamWriter(entityStream, charset));
    }
}
