package de.oth.amp17.bff.bcm.testing.cis.boundary;

import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.param.DateRangeParam;
import com.google.common.base.Preconditions;
import de.oth.amp17.bff.bcm.testing.cis.control.JsonProcedureStatus;
import de.oth.amp17.bff.bcm.testing.cis.control.JsonResourceIdentity;
import org.hl7.fhir.dstu3.model.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.time.LocalDate;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import static javax.ws.rs.core.Response.*;

@ApplicationScoped
@Path("/device")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class DeviceResource {

    private static final Logger LOG = Logger.getLogger(PatientResource.class.getName());

    @Context
    UriInfo uriInfo;

    @Inject
    IGenericClient client;

    @Inject
    IParser parser;

    @GET
    @Path("updatedRecently")
    public Response updatedRecently() {
        Bundle bundle = client.search()
                .forResource(Procedure.class)
                .where(Procedure.CODE.exactly().systemAndCode("http://oth-regensburg.de/amp/procedure", "bcm"))
//                .lastUpdated(new DateRangeParam(LocalDate.now().minusDays(14).toString(), null))
//                .sort().descending("_lastUpdated")
//                .count(20)
                .include(Procedure.INCLUDE_PATIENT)
                .returnBundle(Bundle.class)
                .execute();
        LOG.info("FHIR :: fetched procedures, request: " + bundle.getLink("self").getUrl());

        Map<String, Patient> patients = extractAll(ResourceType.Patient, Patient.class, bundle);

        JsonArray result = bundle.getEntry().stream()
                .filter(this::isProcedure)
                .map(Bundle.BundleEntryComponent::getResource)
                .map(Procedure.class::cast)
                .map(each -> new JsonProcedureStatus(patients.get(each.getSubject().getReference()), each).toJsonObjectBuilder())
                .collect(Json::createArrayBuilder, JsonArrayBuilder::add, JsonArrayBuilder::add)
                .build();

        return ok().entity(result).build();
    }

    @POST
    @Path("observation")
    public Response observation(JsonObject payload) {

        JsonObject jsonQuantity = payload.getJsonObject("qty");
        String patientRef = payload.getString("patientRef");

        Observation observation = new Observation()
                .setSubject(new Reference(patientRef))
                .setCode(new CodeableConcept()
                        .addCoding(new Coding("http://snomed.info/sct", "365812005", "Blood glucose level")))
                .setEffective(new DateTimeType().setValue(new Date()))
                .setValue(new Quantity()
                        .setValue(jsonQuantity.getJsonNumber("value").doubleValue())
                        .setUnit(jsonQuantity.getString("unit")));

        MethodOutcome outcome = client.create()
                .resource(observation)
                .execute();

        return accepted(new JsonResourceIdentity(outcome.getId()).toJson())
                .build();
    }

    @GET
    @Path("{token}")
    public Response findByToken(@PathParam("token") String token) {
        Bundle bundle = client.search()
                .forResource(Procedure.class)
                .where(Procedure.CODE.exactly().systemAndCode("http://oth-regensburg.de/amp/bcm/token", token))
                .include(Procedure.INCLUDE_PATIENT)
                .returnBundle(Bundle.class)
                .execute();

        if (bundle.getTotal() < 2) {
            return status(Status.NOT_FOUND).build();
        }

        Patient patient = extract(ResourceType.Patient, Patient.class, bundle);
        Procedure procedure = extract(ResourceType.Procedure, Procedure.class, bundle);

        return ok(new JsonProcedureStatus(patient, procedure).toJson()).build();

    }

    @POST
    public Response register(JsonObject request) {

        String token = request.getString("token");

        Bundle bundle = client.search()
                .forResource(ProcedureRequest.class)
                .where(ProcedureRequest.CODE.exactly().systemAndCode("http://oth-regensburg.de/amp/bcm/token", token))
                .include(ProcedureRequest.INCLUDE_PATIENT)
                .returnBundle(Bundle.class)
                .execute();

        if (bundle.getTotal() < 2) {
            return serverError().build();
        }

        Patient patient = extract(ResourceType.Patient, Patient.class, bundle);
        ProcedureRequest procedureRequest = extract(ResourceType.ProcedureRequest, ProcedureRequest.class, bundle);

        Procedure procedure = new Procedure();
        procedure.setStatus(Procedure.ProcedureStatus.PREPARATION);
        procedure.setCode(procedureRequest.getCode());
        procedure.setSubject(new Reference(patient.getIdElement().toUnqualifiedVersionless()));
        procedure.setBasedOn(Collections.singletonList(
                new Reference(procedureRequest.getIdElement().toUnqualifiedVersionless())));

        MethodOutcome createOutcome = client.create().resource(procedure).execute();
        Preconditions.checkState(createOutcome.getCreated());
        procedure.setIdElement((IdType) createOutcome.getId());

        procedureRequest.setStatus(ProcedureRequest.ProcedureRequestStatus.COMPLETED);
        MethodOutcome updateOutcome = client.update().resource(procedureRequest).execute();
//        LOG.info(parser.encodeResourceToString(updateOutcome.getOperationOutcome()));
//        Preconditions.checkState(updateOutcome.getCreated());

        return created(uriInfo.getRequestUriBuilder().segment(token).build())
                .entity(new JsonProcedureStatus(patient, procedure).toJson())
                .build();
    }

    private <T> T extract(ResourceType type, Class<T> clazz, Bundle bundle) {
        return bundle.getEntry().stream()
                .filter(each -> hasResourceType(type, each))
                .map(Bundle.BundleEntryComponent::getResource)
                .map(clazz::cast)
                .findFirst().get();
    }

    private <T extends Resource> Map<String, T> extractAll(ResourceType type, Class<T> clazz, Bundle bundle) {
        return bundle.getEntry().stream()
                .filter(each -> hasResourceType(type, each))
                .map(Bundle.BundleEntryComponent::getResource)
                .map(clazz::cast)
                .collect(HashMap::new, (map, each) -> {
                    map.put(each.getIdElement().toUnqualifiedVersionless().getValue(), each);
                }, HashMap::putAll);
    }

    private boolean isProcedure(Bundle.BundleEntryComponent entry) {
        return hasResourceType(ResourceType.Procedure, entry);
    }

    private boolean isPatient(Bundle.BundleEntryComponent entry) {
        return hasResourceType(ResourceType.Patient, entry);
    }

    private boolean hasResourceType(ResourceType resourceType, Bundle.BundleEntryComponent entry) {
        return resourceType == entry.getResource().getResourceType();
    }

}
