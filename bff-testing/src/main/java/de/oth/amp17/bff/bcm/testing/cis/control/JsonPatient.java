package de.oth.amp17.bff.bcm.testing.cis.control;

import com.google.common.base.Preconditions;
import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.dstu3.model.ResourceType;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

public class JsonPatient implements JsonView {

    private Patient patient;


    public JsonPatient(Patient patient) {
        this.patient = patient;
    }

    public static JsonPatient from(Bundle.BundleEntryComponent entry) {
        Resource resource = entry.getResource();

        Preconditions.checkNotNull(resource, "Resource expected");

        if (!ResourceType.Patient.equals(
                resource.getResourceType())) {
            throw new IllegalArgumentException("Expected " + ResourceType.Patient + ", got " + resource.getResourceType());
        }

        return new JsonPatient((Patient) resource);
    }

    @Override
    public JsonObjectBuilder toJsonObjectBuilder() {
        return Json.createObjectBuilder()
                .add("identity", new JsonResourceIdentity(patient).toJsonObjectBuilder())
                .add("lastname", patient.getNameFirstRep().getFamily())
                .add("firstname", patient.getNameFirstRep().getGivenAsSingleString())
                .add("dateOfBirth", patient.getBirthDateElement().getValueAsString());
    }
}
