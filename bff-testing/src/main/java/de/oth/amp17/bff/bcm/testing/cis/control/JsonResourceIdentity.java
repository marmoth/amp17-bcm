package de.oth.amp17.bff.bcm.testing.cis.control;

import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.Resource;
import org.hl7.fhir.instance.model.api.IIdType;

import javax.json.Json;
import javax.json.JsonObjectBuilder;

public class JsonResourceIdentity implements JsonView {

    private IIdType id;

    public JsonResourceIdentity(Bundle.BundleEntryComponent entry) {
        this(new IdType(entry.getFullUrl()));
    }

    public JsonResourceIdentity(Resource resource) {
        this(resource.getIdElement());
    }

    public JsonResourceIdentity(IIdType id) {
        this.id = id;
    }

    @Override
    public JsonObjectBuilder toJsonObjectBuilder() {
        JsonObjectBuilder builder = Json.createObjectBuilder()
                .add("id", id.getIdPart())
                .add("ref", id.toUnqualifiedVersionless().getValue());
        if (id.isAbsolute()) {
            builder.add("full", id.toVersionless().getValue());
        }
        return builder;
    }
}
