package de.oth.amp17.bff.bcm.testing.cis.control;

import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

public interface JsonView {

    default JsonObject toJson() {
        return toJsonObjectBuilder().build();
    }

    JsonObjectBuilder toJsonObjectBuilder();
}
