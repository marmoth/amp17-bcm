package de.oth.amp17.bff.bcm.testing.cis.control;

import com.google.common.base.Preconditions;
import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.Coding;
import org.hl7.fhir.dstu3.model.Procedure;
import org.hl7.fhir.dstu3.model.Procedure.ProcedureStatus;
import org.hl7.fhir.dstu3.model.ProcedureRequest;
import org.hl7.fhir.dstu3.model.ProcedureRequest.ProcedureRequestStatus;

import javax.json.Json;
import javax.json.JsonObjectBuilder;
import java.util.Optional;

public class JsonMonitoringStatus implements JsonView {


    private enum MonitoringState {none, requested, preparation, active, cancelled, completed}

    private final Bundle bundle;

    private Optional<ProcedureRequest> request = Optional.empty();
    private Optional<Procedure> procedure = Optional.empty();

    private ProcedureRequestStatus requestStatus;
    private ProcedureStatus procedureStatus;

    private final MonitoringState monitoringState;

    public JsonMonitoringStatus(Bundle bundle) {
        this.bundle = bundle;
        extractResources(bundle);
        monitoringState = inferState();
    }

    private MonitoringState inferState() {
        requestStatus = request.map(ProcedureRequest::getStatus)
                .orElse(ProcedureRequestStatus.NULL);
        procedureStatus = procedure.map(Procedure::getStatus)
                .orElse(ProcedureStatus.NULL);
        switch (requestStatus) {
            case NULL:
                return MonitoringState.none;
            case DRAFT:
            case ACTIVE:
                return MonitoringState.requested;
            case CANCELLED:
                return MonitoringState.cancelled;
            case COMPLETED:
                switch (procedureStatus) {
                    case NULL:
                        throw new IllegalStateException("Procedure expected");
                    case PREPARATION:
                        return MonitoringState.preparation;
                    case INPROGRESS:
                        return MonitoringState.active;
                    case COMPLETED:
                        return MonitoringState.completed;
                    case ABORTED:
                        return MonitoringState.cancelled;
                }
        }
        throw new IllegalStateException();
    }

    private void extractResources(Bundle bundle) {
        bundle.getEntry().stream()
                .map(Bundle.BundleEntryComponent::getResource)
                .forEach(each -> {
                    switch (each.getResourceType()) {
                        case ProcedureRequest:
                            request = Optional.of((ProcedureRequest) each);
                            break;
                        case Procedure:
                            procedure = Optional.of((Procedure) each);
                            break;
                    }
                });
    }


    @Override
    public JsonObjectBuilder toJsonObjectBuilder() {
        JsonObjectBuilder builder = Json.createObjectBuilder()
                .add("state", monitoringState.name());
        appendRequest(builder);
        appendProcedure(builder);
        return builder;
    }

    private void appendRequest(JsonObjectBuilder builder) {
        if (!request.isPresent()) {
            return;
        }
        builder.add("request", Json.createObjectBuilder()
                .add("state", requestStatus.name())
                .add("requestToken", extractRequestToken())
                .add("identity", new JsonResourceIdentity(request.get()).toJsonObjectBuilder())
        );
    }

    private void appendProcedure(JsonObjectBuilder builder) {
        if (!procedure.isPresent()) {
            return;
        }
        builder.add("procedure", Json.createObjectBuilder()
                .add("state", procedureStatus.name())
                .add("identity", new JsonResourceIdentity(procedure.get()).toJsonObjectBuilder())
        );
    }

    private String extractRequestToken() {
        Preconditions.checkState(request.isPresent());
        return request.flatMap(
                request -> request.getCode().getCoding().stream()
                        .filter(coding -> coding.getSystem().equals("http://oth-regensburg.de/amp/bcm/token"))
                        .map(Coding::getCode)
                        .findFirst())
                .get();
    }

}
