package de.oth.amp17.bff.bcm.testing;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.logging.Logger;

@WebFilter({"/cis/*", "/admin/*", "/device/*"})
public class AngularRoutingFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        Logger.getLogger(AngularRoutingFilter.class.getName()).info("doFilter " + req.getRequestURI());
        req.getRequestDispatcher("/index.html").forward(request, response);
    }

    @Override
    public void destroy() {
    }
}
