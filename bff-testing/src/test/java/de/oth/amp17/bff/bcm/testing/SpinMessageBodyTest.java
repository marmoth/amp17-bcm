package de.oth.amp17.bff.bcm.testing;

import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import com.google.common.collect.Streams;
import org.camunda.spin.Spin;
import org.camunda.spin.json.SpinJsonNode;
import org.junit.Test;

import javax.json.Json;
import javax.mail.internet.ContentType;
import javax.ws.rs.ext.ExceptionMapper;

import static org.junit.Assert.*;

public class SpinMessageBodyTest {

    @Test
    public void parse_content_type() throws Exception {
        ContentType contentType = new ContentType("application/json+fhir;charset=utf-8");

        Streams.stream(
                Iterators.forEnumeration(
                        contentType.getParameterList().getNames())).forEach(System.out::println);
    }

    @Test
    public void json_to_spin() {

        SpinJsonNode spin = Spin.JSON(Json.createObjectBuilder()
                .add("int", 123)
                .add("string", "string value")
                .add("object", Json.createObjectBuilder().add("long", 123))
                .build());

        System.out.println("spin = " + spin);


    }

}