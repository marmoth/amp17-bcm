package de.oth.amp17.bff.bcm.testing.cis.control;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import org.hl7.fhir.dstu3.model.Bundle;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.InputStreamReader;

import static org.junit.Assert.*;

public class JsonMonitoringStatusTest {

    private static FhirContext ctx;

    @BeforeClass
    public static void createFHIRContext() {
        ctx = FhirContext.forDstu3();
    }

    @Test
    public void should_infer_status_active() {
        IParser parser = ctx.newXmlParser();
        Bundle bundle = parser.parseResource(Bundle.class, new InputStreamReader(getClass().getResourceAsStream(
                "FHIR_ProcedureRequest_Bundle.xml")));

        JsonMonitoringStatus monitoringStatus = new JsonMonitoringStatus(bundle);

        System.out.println(monitoringStatus.toJson().toString());
    }

}