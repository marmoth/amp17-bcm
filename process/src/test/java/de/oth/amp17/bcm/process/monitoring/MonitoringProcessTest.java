package de.oth.amp17.bcm.process.monitoring;

import com.google.common.collect.ImmutableMap;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.camunda.bpm.engine.test.mock.Mocks;
import org.camunda.spin.Spin;
import org.camunda.spin.json.SpinJsonNode;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.io.InputStreamReader;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineTests.*;
import static org.mockito.Mockito.mock;

public class MonitoringProcessTest {

    @Rule
    public ProcessEngineRule processEngineRule = new ProcessEngineRule();

    @Before
    public void setup() {
        init(processEngineRule.getProcessEngine());
    }

    @Test
    @Deployment(resources = "bcm.bpmn")
    public void evaluate() throws Exception {

        RuntimeService runtimeService = processEngineRule.getRuntimeService();
        ProcessInstance processInstance = runtimeService.createProcessInstanceByKey("BCM_01")
                .businessKey("BCM::Patient/1")
                .startAfterActivity("SetMonitoringParametersTask").setVariables(ImmutableMap.of(
                        "monitoringRejected", false,
                        "procedureRequestId", "Procedure/100"
                ))
                .startBeforeActivity("CheckMonitoringStatusTask")
                .execute();

        runtimeService.messageEventReceived(
                "ObservationsReceived",
                processInstance.getId(), ImmutableMap.of(
                        "payload", readResource("MSG_ObservationsReceived_01.json")));

        assertThat(processInstance).hasPassed("DummyTask");

        assertThat(processInstance).isWaitingAt("CheckMonitoringStatusTask");
        complete(task("CheckMonitoringStatusTask"), ImmutableMap.of("continueMonitoring", false));

        assertThat(processInstance).isEnded();

    }

    private SpinJsonNode readResource(String resource) throws Exception {
        InputStreamReader reader = new InputStreamReader(
                getClass().getResourceAsStream(resource), "UTF-8");
        return Spin.JSON(reader);
    }

}
