package de.oth.amp17.bcm.process.polling;

import ca.uhn.fhir.model.primitive.DateTimeDt;
import com.google.common.collect.ImmutableMap;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.test.Deployment;
import org.camunda.bpm.engine.test.ProcessEngineRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.Date;

import static org.camunda.bpm.engine.test.assertions.ProcessEngineAssertions.assertThat;
import static org.camunda.bpm.engine.test.assertions.ProcessEngineAssertions.init;

public class HistoryPollerProcessTest {

    @Rule
    public ProcessEngineRule processEngineRule = new ProcessEngineRule();

    @Before
    public void setup() {
        init(processEngineRule.getProcessEngine());
    }

    @Test
    @Deployment(resources = "fhir-poll.bpmn")
    public void evaluate() throws Exception {

        RuntimeService runtimeService = processEngineRule.getRuntimeService();
        ProcessInstance processInstance = runtimeService.createProcessInstanceByKey("FHIR-POLL")
                .setVariables(ImmutableMap.of(
                        "fhirServerURL", "http://localhost:8090/baseDstu3",
                        "historySince", new DateTimeDt(new Date())
                ))
                .execute();

        assertThat(processInstance).isWaitingAt("DummyTask");

    }

}
