package de.oth.amp17.bcm.process;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import org.camunda.spin.Spin;
import org.camunda.spin.json.SpinJsonNode;
import org.camunda.spin.xml.SpinXmlElement;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.exceptions.FHIRException;
import org.junit.Test;

import java.io.InputStreamReader;
import java.math.BigDecimal;

public class FhirExploration {

    private SpinJsonNode observations = Spin.JSON("[]");

    @Test
    public void explore_communication_resource() {
        Communication communication;
    }

    @Test
    public void process_xml_history() throws Exception {
        SpinXmlElement root = readXmlResource("FHIR_history.xml");
        root.xPath("./f:entry[f:request/f:method/@value = 'POST' or f:request/f:method/@value = 'PUT']/f:resource/*")
                .ns("f", "http://hl7.org/fhir")
                .elementList().forEach(each -> {
                    switch (each.name()) {
                        case "Observation":
                            System.out.println("Observation = " + each);
                            break;
                        case "EpisodeOfCare": {
                            System.out.println("EpisodeOfCare = " + each);
                            break;
                        }
                    }
                }
        );
    }

    @Test
    public void process_json_history() throws Exception {
        SpinJsonNode root = readJsonResource("FHIR_history.json");

        Multimap<String, SpinJsonNode> observations = LinkedHashMultimap.create();

        root.jsonPath("$.entry[?(@request.method == 'PUT')].resource").elementList().forEach(resource -> {
            System.out.println("resource = " + resource);
            String type = resource.prop("resourceType").stringValue();
            switch (type) {
                case "Observation": {
                    String patientId = resource.jsonPath("$.subject.reference").stringValue();
                    observations.put(patientId, resource);
                    break;
                }

            }
        });


    }

    @Test
    public void fetch_history() {

        FhirContext ctx = FhirContext.forDstu3();
        IGenericClient client = ctx.newRestfulGenericClient("http://localhost:8090/baseDstu3");

        Bundle bundle = client.history().onServer()
                .andReturnBundle(Bundle.class)
                .execute();

//        ctx.newFluentPath().evaluate(bundle, "entry.where(request.method = 'PUT')", Resource.class).forEach(System.out::println);
//        System.out.println("---");
        ctx.newFluentPath().evaluate(bundle, "entry.where(request.method in ('PUT' | 'POST')).select(resource as Observation)", Resource.class).forEach(System.out::println);

        bundle.getEntry().forEach(this::handle);

        System.out.println("observations = " + observations);

    }

    private void handle(Bundle.BundleEntryComponent component) {
        Resource resource = component.getResource();
        System.out.println("resource = " + resource.getResourceType().name());

        switch (resource.getResourceType().name()) {
            case "Observation":
                handleObservation((Observation) resource);
                break;
            default:
        }

    }

    private void handleObservation(Observation observation) {
        Coding coding = observation.getCode().getCodingFirstRep();
        if ("http://snomed.info/sct".equals(coding.getSystem())
                && "365812005".equals(coding.getCode())) {

            try {
                Quantity quantity = observation.getValueQuantity();
                BigDecimal value = quantity.getValue();
                String unit = quantity.getUnit();
                System.out.println("value = " + value);
                System.out.println("unit = " + unit);

                SpinJsonNode node = Spin.JSON("{}").prop("qty",
                        ImmutableMap.of(
                                "value", value,
                                "unit", unit
                        )
                );
                System.out.println(node.toString());

                observations.insertAt(0, node);

            } catch (FHIRException e) {
                throw new RuntimeException(e);
            }

        }
    }

    private SpinJsonNode readJsonResource(String resource) throws Exception {
        InputStreamReader reader = new InputStreamReader(
                getClass().getResourceAsStream(resource), "UTF-8");
        return Spin.JSON(reader);
    }

    private SpinXmlElement readXmlResource(String resource) throws Exception {
        InputStreamReader reader = new InputStreamReader(
                getClass().getResourceAsStream(resource), "UTF-8");
        return Spin.XML(reader);
    }

}
