package de.oth.amp17.bcm.process.polling.internal;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import org.junit.Test;

import java.time.Instant;

public class FhirHistoryTest {

    @Test
    public void fetch_and_map() {
        FhirContext ctx = FhirContext.forDstu3();
        IGenericClient client = ctx.newRestfulGenericClient("http://localhost:8090/baseDstu3");

        MessageDispatcher dispatcher = new ToSystemOutDispatcher();

        FhirHistory history = new FhirHistory(client, dispatcher);
        Instant lastPolled = history.fetchAndDispatch();
        System.out.println("lastPolled = " + lastPolled);

    }

}