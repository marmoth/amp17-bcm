package de.oth.amp17.bcm.process.testing.boundary;

import org.camunda.bpm.engine.RuntimeService;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class MonitoringTestingResourceTest {

    @Test
    public void should_return_businessId() {
        MonitoringTestingResource resource = new MonitoringTestingResource();
        resource.engine = mock(RuntimeService.class);

        int patientId = 12134;

        String businessKey = resource.monitoringRequested(patientId);

        assertEquals("bcm::" + patientId, businessKey);

    }

}