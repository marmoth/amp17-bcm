package de.oth.amp17.bcm.process.polling.internal;

import javax.json.JsonObject;

public class ToSystemOutDispatcher implements MessageDispatcher {

    @Override
    public void dispatchObservation(JsonObject payload) {
        System.out.printf("Observation :: %s%n", payload);
    }

    @Override
    public void dispatchMonitoringRequest(JsonObject payload) {
        System.out.printf("MonitoringRequest :: %s%n", payload);
    }

    @Override
    public void dispatchProcedure(JsonObject payload) {
        System.out.printf("Procedure :: %s%n", payload);
    }
}
