package de.oth.amp17.bcm.process.control;

import javax.ejb.ConcurrencyManagement;
import javax.ejb.ConcurrencyManagementType;
import javax.ejb.Singleton;
import javax.inject.Named;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Named
@Singleton
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class MonitoringRegistry {

    private Map<String, String> patientToBusinessKey = new ConcurrentHashMap<>();

    public void add(String patientId, String businessKey) {
        patientToBusinessKey.put(patientId, businessKey);
    }

    public void remove(String patientId) {
        patientToBusinessKey.remove(patientId);
    }

    public boolean monitoringActive(String patientId) {
        return patientToBusinessKey.containsKey(patientId);
    }

}
