package de.oth.amp17.bcm.process.polling.internal;

import com.google.common.base.Preconditions;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import com.google.common.collect.Streams;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.instance.model.api.IIdType;

import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import java.util.*;
import java.util.stream.Stream;

class HistoryBundleEntryMapper {

    private static final String CODE_Postprandial = "24863003";
    private static final String CODE_Preprandial = "307165006";
    private static final String PROP_QUALIFIER = "qualifier";

    private final LinkedListMultimap<String, JsonObjectBuilder> patientObservations = LinkedListMultimap.create();

    private List<JsonObject> monitoringRequests = new LinkedList<>();

    private List<JsonObject> procedures = new LinkedList<>();

    HistoryBundleEntryMapper() {
    }

    private Stream<JsonObject> patientObservations() {
        return patientObservations.asMap().entrySet().stream()
                .map(this::toObservationsMessage);
    }

    private Stream<JsonObject> monitoringRequests() {
        return monitoringRequests.stream();
    }

    private Stream<JsonObject> procedures() {
        return procedures.stream();
    }

    void dispatchTo(MessageDispatcher dispatcher) {
        monitoringRequests().forEach(dispatcher::dispatchMonitoringRequest);
        procedures().forEach(dispatcher::dispatchProcedure);
        patientObservations().forEach(dispatcher::dispatchObservation);
    }

    private JsonObject toObservationsMessage(Map.Entry<String, Collection<JsonObjectBuilder>> entry) {
        List<JsonObjectBuilder> observations = Lists.reverse(Lists.newArrayList(entry.getValue()));
        return Json.createObjectBuilder()
                .add("messageType", "observations")
                .add("patientId", entry.getKey())
                .add("observations", observations.stream()
                        .collect(Json::createArrayBuilder, JsonArrayBuilder::add, JsonArrayBuilder::add))
                .build();
    }

    public void append(Bundle.BundleEntryComponent entry) {
        Resource resource = entry.getResource();
        switch (resource.getResourceType()) {
            case Observation:
                onObservation((Observation) resource);
                break;
            case ProcedureRequest:
                onProcedureRequest((ProcedureRequest) resource);
                break;
            case Procedure:
                onProcedure((Procedure) resource);
                break;
        }
    }

    private void onProcedure(Procedure procedure) {
        if (!isMonitoringProcedure(procedure)) {
            return;
        }
        IIdType id = procedure.getSubject().getReferenceElement();

        String status = procedure.getStatus().toCode();

        procedures.add(Json.createObjectBuilder()
                .add("messageType", "procedure")
                .add("procedure", Json.createObjectBuilder()
                        .add("id", id.getIdPart())
                        .add("status", status)
                ).build());
    }

    private void onProcedureRequest(ProcedureRequest request) {
        if (!isMonitoringRequest(request)) {
            return;
        }
        IIdType id = request.getSubject().getReferenceElement();

        Preconditions.checkArgument(id.hasIdPart(), "Id part expected");
        Preconditions.checkArgument(id.hasResourceType(), "ResourceType expected");

        monitoringRequests.add(Json.createObjectBuilder()
                .add("messageType", "monitoringRequest")
                .add("patient", Json.createObjectBuilder()
                        .add("id", id.getIdPart())
                        .add("ref", id.toUnqualifiedVersionless().getValue())).build());
    }

    private void onObservation(Observation observation) {
        if (!isBloodGlucoseMeasurement(observation)) {
            return;
        }

//        if (!isMonitoringActiveForPatient(observation.getSubject().getReferenceElement())) {
//            return;
//        }

        JsonObjectBuilder builder = Json.createObjectBuilder();

        Quantity quantity;
        try {
            quantity = observation.getValueQuantity();
        } catch (FHIRException e) {
            throw new RuntimeException(e);
        }

        builder.add("quantity", Json.createObjectBuilder()
                .add("value", quantity.getValue())
                .add("unit", quantity.getUnit())
        );

        observation.getCode().getCoding().forEach(each -> {
            switch (each.getSystem()) {
                case "http://snomed.info/sct":
                    switch (each.getCode()) {
                        case CODE_Postprandial:
                            builder.add(PROP_QUALIFIER, "postprandial");
                            break;
                        case CODE_Preprandial:
                            builder.add(PROP_QUALIFIER, "preprandial");
                            break;
                        default:
                            builder.addNull(PROP_QUALIFIER);
                    }
                    break;
                case "http://hl7.org/fhir/v3/TimingEvent":
                    builder.add("timing", each.getCode());
                    break;
            }
        });

        String reference = observation.getSubject().getReference();
        if (reference == null) {
            return;
        }
        patientObservations.put(
                reference,
                builder);

    }

    private boolean isBloodGlucoseMeasurement(Observation observation) {
        return hasCode(observation.getCode(),
                "http://snomed.info/sct",
                "365812005");
    }

    private boolean isMonitoringProcedure(Procedure request) {
        return hasCode(request.getCode(),
                "http://oth-regensburg.de/amp/procedure",
                "bcm");
    }

    private boolean isMonitoringRequest(ProcedureRequest request) {
        return hasCode(request.getCode(),
                "http://oth-regensburg.de/amp/procedure",
                "bcm");
    }

    private boolean hasCode(CodeableConcept codeableConcept, String system, String code) {
        return codeableConcept.getCoding().stream()
                .filter(each -> {
                    return system.equals(each.getSystem()) && code.equals(each.getCode());
                })
                .findFirst()
                .isPresent();
    }

    private boolean isMonitoringActiveForPatient(IIdType patientId) {
        return true;
//        Preconditions.checkArgument(
//                ResourceType.Patient.name().equals(patientId.getResourceType()),
//                "Patient id expected");
//        Preconditions.checkArgument(patientId.hasIdPart(), "Id part missing");
//
//        return monitoringRegistry.monitoringActive(
//                patientId.getIdPart()
//        );
    }

}
