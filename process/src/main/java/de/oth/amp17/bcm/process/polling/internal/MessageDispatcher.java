package de.oth.amp17.bcm.process.polling.internal;

import javax.json.JsonObject;

public interface MessageDispatcher {

    void dispatchObservation(JsonObject payload);

    void dispatchMonitoringRequest(JsonObject payload);

    void dispatchProcedure(JsonObject payload);
}
