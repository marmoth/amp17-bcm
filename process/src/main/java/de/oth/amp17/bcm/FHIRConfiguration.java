package de.oth.amp17.bcm;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.client.api.IGenericClient;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.inject.Produces;

@Singleton
@Startup
public class FHIRConfiguration {

    private static final String FHIR_BASE_URL = "http://localhost:8090/baseDstu3";

    private FhirContext context;

    private IGenericClient client;

    @PostConstruct
    void initFhirClient() {
        context = FhirContext.forDstu3();
        client = context.newRestfulGenericClient(FHIR_BASE_URL);
    }

    @Produces
    public IGenericClient client() {
        return client;
    }

    @Produces
    public IParser jsonParser() {
        IParser parser = context.newJsonParser();
        parser.setPrettyPrint(true);
        parser.setServerBaseUrl(FHIR_BASE_URL);
        return parser;
    }

}
