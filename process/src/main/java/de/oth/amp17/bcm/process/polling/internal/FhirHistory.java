package de.oth.amp17.bcm.process.polling.internal;

import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.IHistoryTyped;
import org.hl7.fhir.dstu3.model.*;

import java.time.Instant;
import java.util.Date;
import java.util.Optional;

public class FhirHistory {

    private final IGenericClient client;

    private final MessageDispatcher dispatcher;

    public FhirHistory(IGenericClient client, MessageDispatcher dispatcher) {
        this.client = client;
        this.dispatcher = dispatcher;
    }

    public Instant fetchAndDispatch() {
        return doFetchAndDispatch(Optional.empty());
    }

    public Instant fetchAndDispatch(Instant lastPolled) {
        return doFetchAndDispatch(Optional.of(lastPolled));
    }

    private Instant doFetchAndDispatch(Optional<Instant> lastPolled) {
        HistoryBundleEntryMapper mapper = new HistoryBundleEntryMapper();
        Bundle bundle = fetchHistory(lastPolled);
        bundle.getEntry().stream()
                .forEach(mapper::append);
        mapper.dispatchTo(dispatcher);

        Meta meta = (Meta) bundle.getChildByName("meta").getValues().get(0);
        Property propLastUpdated = meta.getChildByName("lastUpdated");
        InstantType lastUpdated = (InstantType) propLastUpdated.getValues().get(0);
        return lastUpdated.getValue().toInstant();
    }

    private Bundle fetchHistory(Optional<Instant> lastPolled) {

        IHistoryTyped<Bundle> historyTyped = client.history()
                .onServer()
                .andReturnBundle(Bundle.class);
        Bundle bundle = lastPolled
                .map(x -> new Date(x.toEpochMilli()))
                .map(historyTyped::since).orElse(historyTyped)
                .execute();
        BundleFetcher.fetchRestOfBundle(client, bundle);
        return bundle;
    }
}
