package de.oth.amp17.bcm.process.testing.boundary;

import com.google.common.collect.ImmutableMap;
import org.camunda.bpm.engine.RuntimeService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@ApplicationScoped
@Path("testing")
public class MonitoringTestingResource {

    @Inject
    RuntimeService engine;

    @GET
    @Path("monitoringRequested/{patientId}")
    public String monitoringRequested(@PathParam("patientId") long patientId) {
        String businessKey = patientIdToBusinessKey(patientId);
        engine.startProcessInstanceByMessage(
                "???",
                businessKey,
                ImmutableMap.of(
                        "variable1", "????",
                        "variable2", "????"
                )
        );
        return businessKey;
    }

    @GET
    @Path("qrcodeScannedByApp/{patientId}/{registrationCode}")
    public void qrcodeScannedByApp(
            @PathParam("patientId") long patientId,
            @PathParam("registrationCode") String registrationCode) {

        String businessKey = patientIdToBusinessKey(patientId);
        engine.correlateMessage(
                "???",
                businessKey,
                ImmutableMap.of(
                        "registrationCode", registrationCode,
                        "variable2", "????"
                )
        );

    }

    private String patientIdToBusinessKey(long patientId) {
        return String.format("bcm::%d", patientId);
    }

}
